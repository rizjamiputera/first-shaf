import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Enviroment {
  static String get fileName {
    // load production enviroment
    if (kReleaseMode) return '.env';

    // instead load development enviroment
    return '.env.development';
  }

  static String get apiURL {
    return dotenv.get('API_URL');
  }

  static String get gMapsKey {
    return dotenv.get('MAPS_API_KEY');
  }
}
