class Mosque {
  final String id;
  final String name;
  final String rating;
  final String address;
  final String full_address;
  final String phone_number;
  final double latitude;
  final double longitude;
  final dynamic viewport;
  final String distance;
  final String photo;
  final List<String> photos;
  final bool openNow;
  final List<Map<String, dynamic>> reviews;
  final bool wheelchair_accessible_entrance;

  Mosque({
    required this.id,
    required this.name,
    required this.rating,
    required this.address,
    required this.full_address,
    required this.phone_number,
    required this.latitude,
    required this.longitude,
    required this.viewport,
    required this.distance,
    required this.photo,
    required this.photos,
    required this.openNow,
    required this.reviews,
    required this.wheelchair_accessible_entrance,
  });
}
