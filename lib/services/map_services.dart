import 'dart:convert';

import 'package:first_shaf/models/enviroments.dart';
import 'package:first_shaf/models/mosque.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:maps_toolkit/maps_toolkit.dart' as maps_toolkit;

class MapServices {
  static SharedPreferences? _prefs;
  static final String key = Enviroment.gMapsKey;
  final String types = 'geocode';

  static const _user_longitude = 'user_longitude';
  static const _user_latitude = 'user_latitude';
  static const _user_address_json = 'user_address_json';
  static const _mosque_list_json = 'mosque_list_json';

  static Future init() async => _prefs = await SharedPreferences.getInstance();

  static Future getCurrentLocation() async {
    final Location location = Location();
    LocationData location_data = await location.getLocation();

    _prefs!.setDouble(_user_longitude, location_data.longitude!);
    _prefs!.setDouble(_user_latitude, location_data.latitude!);

    Map<String, dynamic> address =
        await getAddress(location_data.latitude!, location_data.longitude!);

    _prefs!.setString(_user_address_json, jsonEncode(address));
  }

  static String getPhotoUrl(String photoReference, int maxWidth) {
    final String apiUrl = "https://maps.googleapis.com/maps/api/place/photo";

    final Uri uri = Uri.parse(
        "$apiUrl?maxwidth=$maxWidth&photoreference=$photoReference&key=$key");

    return uri.toString();
  }

  static Future<Map<String, dynamic>> getAddress(
      double latitude, double longitude) async {
    final String apiUrl = "https://maps.googleapis.com/maps/api/geocode/json";
    final String location = "$latitude,$longitude";
    final Uri uri = Uri.parse("$apiUrl?latlng=$location&key=$key");

    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final data = json.decode(response.body);

      final List<dynamic> results = data['results'];

      if (results.isNotEmpty) {
        var components = results[0]['address_components'];
        String locality = '';
        String subAdministrativeArea = '';

        for (var component in components) {
          List<String> types = List<String>.from(component['types']);
          if (types[0] == 'administrative_area_level_3') {
            locality = component['short_name'];
          }
          if (types[0] == 'administrative_area_level_2') {
            subAdministrativeArea = component['short_name'];
          }
        }

        return {
          'formatted_address': results[0]['formatted_address'],
          'locality': locality,
          "subAdministrativeArea": subAdministrativeArea
        };
      }
    }
    return {
      'formatted_address': "",
      'locality': "",
      "subAdministrativeArea": ""
    };
  }

  static Future getMosqueNearby(double radius, int maxPhotoWidth) async {
    double? latitude = _prefs!.getDouble(_user_latitude);
    double? longitude = _prefs!.getDouble(_user_longitude);

    final String apiUrl =
        "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
    final String args = 'type=mosque';
    final String location = "$latitude,$longitude";

    final Uri uri =
        Uri.parse("$apiUrl?$args&location=$location&radius=$radius&key=$key");

    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final List<dynamic> results = data['results'];

      // START COLLECT DETAILS IN PARALEL

      const int maxParallelRequests = 5; // limit for paralel request
      List<Future<Map<String, dynamic>>> detailFutures = [];
      List<Map<String, dynamic>> processedMosques = [];

      for (var i = 0; i < results.length; i++) {
        var result = results[i];
        if (detailFutures.length >= maxParallelRequests) {
          // Wait for the current batch of requests to complete
          List<Map<String, dynamic>> details = await Future.wait(detailFutures);
          processedMosques.addAll(details);
          detailFutures.clear();
        }

        detailFutures.add(getMosqueDetail(result["place_id"], maxPhotoWidth));
      }

      // Wait for the remaining detail requests to complete
      List<Map<String, dynamic>> details = await Future.wait(detailFutures);
      processedMosques.addAll(details);

      // END COLLECT DETAILS IN PARALEL

      List<Map<String, dynamic>> mosques = [];

      for (var i = 0; i < results.length; i++) {
        var result = results[i];
        Map<String, dynamic> details = processedMosques[i];
        // Map<String, dynamic> details =
        //     await getMosqueDetail(result["place_id"], maxPhotoWidth);

        String photo = '';
        List<String> photos = [];

        // merge photos from detail
        if (result.containsKey("photos")) {
          List<Map<String, dynamic>> _photos =
              List<Map<String, dynamic>>.from(result["photos"]);

          if (_photos.isNotEmpty) {
            for (var i = 0; i < _photos.length; i++) {
              Map<String, dynamic> photo_data = _photos[i];

              int _maxWidth = maxPhotoWidth < photo_data["width"]
                  ? maxPhotoWidth
                  : photo_data["width"];

              String photo_url =
                  getPhotoUrl(photo_data["photo_reference"], _maxWidth);

              photos.add(photo_url);
            }

            photo = photos[0];
          }
        }

        // DISTANCE
        double mosque_latitude = result["geometry"]["location"]["lat"];
        double mosque_longitude = result["geometry"]["location"]["lng"];
        Map<String, dynamic> distance =
            getDistance(latitude, longitude, mosque_latitude, mosque_longitude);

        final mosque = {
          "id": result["place_id"],
          "name": result["name"],
          "rating":
              result["rating"] != null ? result["rating"].toString() : "0",
          "address": result["vicinity"],
          "full_address": details["full_address"],
          "phone_number": details["phone_number"] ?? "",
          "latitude": mosque_latitude,
          "longitude": mosque_longitude,
          "viewport": result["geometry"]["viewport"],
          "distance": distance["read_distance"],
          "photo": photo,
          "photos": details["photos"],
          "wheelchair_accessible_entrance":
              details["wheelchair_accessible_entrance"],
          "reviews": details["reviews"] ?? [],
          "openNow": result.containsKey("opening_hours")
              ? result["opening_hours"]["open_now"]
              : false
        };

        mosques.add(mosque);
      }

      _prefs!.setString(_mosque_list_json, jsonEncode(mosques));
    }
  }

  static Map<String, dynamic> getDistance(
      from_latitude, from_longitude, to_latitude, to_longitude) {
    maps_toolkit.LatLng from =
        maps_toolkit.LatLng(from_latitude, from_longitude);
    maps_toolkit.LatLng to = maps_toolkit.LatLng(to_latitude, to_longitude);

    num _distance_in_meter =
        maps_toolkit.SphericalUtil.computeDistanceBetween(from, to);
    String distance_in_meter = _distance_in_meter.toStringAsFixed(0);

    num _distance_in_km = _distance_in_meter / 1000;
    String distance_in_km = _distance_in_km.toStringAsFixed(1);

    String read_distance =
        _distance_in_km > 1 ? distance_in_km + ' km' : distance_in_meter + ' m';

    return {
      "distance": _distance_in_km,
      "read_distance": read_distance,
    };
  }

  static Future<Map<String, dynamic>> getMosqueDetail(
      String placeId, int maxPhotoWidth) async {
    final String apiUrl =
        "https://maps.googleapis.com/maps/api/place/details/json";
    final String args = 'place_id=$placeId';

    final Uri uri = Uri.parse("$apiUrl?$args&key=$key");
    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      final Map<String, dynamic> result = data['result'];
      List<String> photos = [];

      if (result.containsKey("photos")) {
        List<Map<String, dynamic>> _photos =
            List<Map<String, dynamic>>.from(result["photos"]);

        if (_photos.isNotEmpty) {
          for (var i = 0; i < _photos.length; i++) {
            Map<String, dynamic> photo = _photos[i];

            int _maxWidth =
                maxPhotoWidth < photo["width"] ? maxPhotoWidth : photo["width"];

            String photo_url = getPhotoUrl(photo["photo_reference"], _maxWidth);

            photos.add(photo_url);
          }
        }
      }

      bool wheelchair_accessible_entrance =
          result.containsKey("wheelchair_accessible_entrance")
              ? result["wheelchair_accessible_entrance"]
              : false;
      final details = {
        "full_address": result["formatted_address"],
        "phone_number": result["formatted_phone_number"],
        "photos": photos,
        "wheelchair_accessible_entrance": wheelchair_accessible_entrance,
        "reviews": result["reviews"]
      };

      return details;
    }
    return {};
  }

  static Future<List<LatLng>> getPolylines(double latitude, double longitude,
      [double u_latitude = 0, double u_longitude = 0]) async {
    double? user_latitude =
        u_latitude != 0 ? u_latitude : _prefs!.getDouble(_user_latitude);
    double? user_longitude =
        u_longitude != 0 ? u_longitude : _prefs!.getDouble(_user_longitude);

    PointLatLng sourcePoint = PointLatLng(user_latitude!, user_longitude!);
    PointLatLng destPoint = PointLatLng(latitude, longitude);

    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        key, sourcePoint, destPoint,
        travelMode: TravelMode.driving);

    List<LatLng> polylineCoordinates = [];

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    return polylineCoordinates;
  }

  static double? get_user_latitude() => _prefs!.getDouble(_user_latitude);

  static double? get_user_longitude() => _prefs!.getDouble(_user_longitude);

  static Map<String, dynamic> get_user_address() {
    String? address_json = _prefs!.getString(_user_address_json);
    return jsonDecode(address_json!);
  }

  static List<Mosque> getMosques() {
    List<Mosque> mosques = [];
    String? mosque_list_json = _prefs!.getString(_mosque_list_json);

    if (mosque_list_json != null) {
      List<dynamic> _mosques = jsonDecode(mosque_list_json);

      for (var result in _mosques) {
        final mosque = Mosque(
            id: result["id"],
            name: result["name"],
            rating: result["rating"],
            address: result["address"],
            full_address: result["full_address"],
            phone_number: result["phone_number"],
            latitude: result["latitude"],
            longitude: result["longitude"],
            viewport: result["viewport"],
            distance: result["distance"],
            photo: result["photo"],
            photos: List<String>.from(result["photos"]),
            openNow: result["openNow"],
            reviews: List<Map<String, dynamic>>.from(result["reviews"]),
            wheelchair_accessible_entrance:
                result["wheelchair_accessible_entrance"]);

        mosques.add(mosque);
      }
    }
    return mosques;
  }
}
