import 'dart:async';
import 'package:first_shaf/models/mosque.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:first_shaf/screen/mosque/mosque_detail.dart';

class MosqueSlide extends StatefulWidget {
  final List<Mosque> mosques;
  final selected_mosque_index;
  final Function update_index;
  final Function navigate;
  final bool isDarkMode;
  final CarouselController controller;

  MosqueSlide(
      {required this.mosques,
      required this.selected_mosque_index,
      required this.update_index,
      required this.navigate,
      required this.isDarkMode,
      required this.controller});

  @override
  _MosqueSlideState createState() => _MosqueSlideState();
}

class _MosqueSlideState extends State<MosqueSlide> {
  Timer? _debounceTimer;

  @override
  Widget build(BuildContext context) {
    List<Mosque> mosques = widget.mosques;
    var selected_mosque_index = widget.selected_mosque_index;
    Function update_index = widget.update_index;
    Function navigate = widget.navigate;
    bool isDarkMode = widget.isDarkMode;
    CarouselController controller = widget.controller;

    return Column(
      children: [
        Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.4,
                height: 5,
                decoration: BoxDecoration(
                    color: Color(0xFFBCBCBC),
                    borderRadius: BorderRadius.all(Radius.circular(20))))),
        Text("Mosque around you",
            style: TextStyle(
                fontFamily: "Poppins",
                fontSize: 16,
                fontWeight: FontWeight.w400)),
        Text("Select the mosque you want to visit",
            style: TextStyle(
                fontFamily: "Poppins",
                fontSize: 12,
                fontWeight: FontWeight.w200)),
        mosques.isNotEmpty
            ? CarouselSlider(
                carouselController: controller,
                options: CarouselOptions(
                    autoPlay: false,
                    initialPage: selected_mosque_index,
                    enableInfiniteScroll: false,
                    aspectRatio: 3,
                    onPageChanged: (index, reason) {
                      _debounceTimer?.cancel();
                      _debounceTimer =
                          Timer(const Duration(milliseconds: 500), () {
                        update_index(index);
                      });
                    }),
                items: buildMosqueWidgets(context),
              )
            : Container(height: 100, child: Center(child: Text("Loading.."))),
        mosques.isEmpty
            ? Container()
            : Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Row(children: [
                  Expanded(
                    child: Material(
                      color: Color.fromARGB(255, 190, 144, 6),
                      borderRadius: BorderRadius.circular(50),
                      child: InkWell(
                        onTap: () {
                          navigate();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xFFCBA64B),
                              border: Border.all(color: Color(0xFFE3C478)),
                              borderRadius: BorderRadius.circular(50),
                              boxShadow: [
                                BoxShadow(
                                    color: isDarkMode
                                        ? Colors.grey.shade700.withOpacity(0.5)
                                        : Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 5,
                                    offset: Offset(0, 3))
                              ]),
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 20),
                          child: Row(children: [
                            Text("Navigate",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700)),
                            Spacer(),
                            Image.asset("icons/vector.png",
                                width: 17, height: 17)
                          ]),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20.0, 8.0, 8.0, 8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: Colors.grey, width: 1),
                          borderRadius: BorderRadius.all(Radius.circular(50))),
                      child: Padding(
                        padding: const EdgeInsets.all(5),
                        child: SvgPicture.asset("icons/heart.svg",
                            height: 28, width: 28),
                      ),
                    ),
                  )
                ]),
              ),
      ],
    );
  }

  List<Widget> buildMosqueWidgets(BuildContext context) {
    List<Mosque> mosques = widget.mosques;
    Function navigate = widget.navigate;
    bool isDarkMode = widget.isDarkMode;

    return mosques.map((mosque) {
      int index = mosques.indexOf(mosque);
      return GestureDetector(
        onTap: () {
          // int index = mosques.indexOf(mosque);
          // print(index);

          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => mosqueDetail(
                  mosque: mosque,
                  index: index,
                  onPopCallback: (do_navigate) {
                    if (do_navigate) {
                      navigate();
                    }
                  })));
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Container(
                height: 82,
                decoration: BoxDecoration(
                    color: isDarkMode ? Colors.grey[800] : Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(13)),
                    boxShadow: [
                      BoxShadow(
                          color: isDarkMode
                              ? Colors.grey.shade700.withOpacity(0.5)
                              : Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 5,
                          offset: Offset(0, 3))
                    ]),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Container(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                      Color(0x8ACBA64B),
                                      Color(0x5AFFFFFF)
                                    ], // Gradient colors
                                        begin: Alignment
                                            .topLeft, // Gradient start point
                                        end: Alignment
                                            .bottomRight // Gradient end point
                                        )),
                                child: Hero(
                                    tag: "mosque" +
                                        mosques.indexOf(mosque).toString(),
                                    child: mosque.photo != ''
                                        ? CachedNetworkImage(
                                            imageUrl: mosque.photo,
                                            imageBuilder: (context,
                                                    imageProvider) =>
                                                Container(
                                                  width: 60,
                                                  height: 60,
                                                  decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                          image: imageProvider,
                                                          fit: BoxFit.cover)),
                                                ),
                                            placeholder: (context, url) =>
                                                SvgPicture.asset(
                                                    "icons/mosque.svg",
                                                    height: 60,
                                                    width: 60),
                                            errorWidget: (context, url,
                                                    error) =>
                                                SvgPicture.asset(
                                                    "icons/mosque.svg",
                                                    height: 60,
                                                    width: 60))
                                        : SvgPicture.asset("icons/mosque.svg",
                                            height: 60, width: 60)),
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              height: 62,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    mosque.name,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 13,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Row(
                                      children: [
                                        SvgPicture.asset("icons/star.svg",
                                            height: 16, width: 16),
                                        Text(mosque.rating,
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 10)),
                                        SizedBox(width: 5),
                                        SvgPicture.asset("icons/location.svg"),
                                        Text(mosque.distance,
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 10)),
                                        SizedBox(width: 5),
                                        SvgPicture.asset("icons/login2.svg",
                                            height: 16, width: 16),
                                        Text(mosque.openNow ? "Open" : "Close",
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 10)),
                                      ],
                                    ),
                                  ),
                                  Text(
                                    mosque.address,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: isDarkMode
                                            ? Colors.white
                                            : Colors.grey[700],
                                        fontSize: 10,
                                        fontFamily: "Poppins"),
                                  ),
                                  index == 0
                                      ? Expanded(
                                          child: Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Text(
                                            "Nearest",
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                color: Color(0xFFCBA64B),
                                                fontSize: 10,
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ))
                                      : Text("")
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }).toList();
  }
}
