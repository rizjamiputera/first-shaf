import 'dart:async';
import 'package:first_shaf/models/mosque.dart';
import 'package:first_shaf/screen/dashboard/bottom_container.dart';
import 'package:first_shaf/screen/dashboard/google_map.dart';
import 'package:first_shaf/screen/dashboard/has_arrived.dart';
import 'package:first_shaf/screen/dashboard/mosque_slide.dart';
import 'package:first_shaf/screen/dashboard/mosques_card.dart';
import 'package:first_shaf/screen/dashboard/navigation_info.dart';
import 'package:first_shaf/screen/dashboard/pray_details.dart';
import 'package:first_shaf/screen/welcome.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:first_shaf/services/map_services.dart';
import 'package:location/location.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:adhan_dart/adhan_dart.dart';
import 'package:flutter_timezone/flutter_timezone.dart';
import 'package:intl/intl.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreen createState() => _DashboardScreen();
}

class _DashboardScreen extends State<DashboardScreen>
    with TickerProviderStateMixin {
  bool isDarkMode = false;
  Brightness systemBrightness = Brightness.light;
  bool usePlatformBrightness = true;
  bool onNavigation = false;
  late StreamSubscription<LocationData> locationSubscription;
  String speed = '0 km/h';
  String distance = '0 km';
  String duration = '';

  // NAVIGATING DURATION
  late DateTime startTimeNav;
  late DateTime endTimeNav;
  late Duration duration_navigate;
  String read_duration_navigate = '';
  bool hasArrived = false;
  // String navigate_duration = '5 minutes';

  LocationData? currentLocation;
  List<Mosque> mosques = [];
  bool noMosqueAround = false;
  var card_mosque_index = 0;
  var selected_mosque_index = null;

  // GOOGLE MAP
  GoogleMapController? mapController;
  Set<Marker> _markers = {};
  Set<Circle> _circles = {};
  Set<Polyline> _polylines = {};

  // PRAY TIMES
  String currentTimeZone = "";
  String timeNow = DateFormat('HH:mm').format(DateTime.now());
  List<Map<String, dynamic>> prayerTimes = [];
  String nextPrayName = '';
  String nextPrayTime = '';
  DateTime nextPrayerTime = DateTime.now();
  String diffPrayerTime = '';

  // CUSTOM MARKERS
  BitmapDescriptor markerIcon = BitmapDescriptor.defaultMarker;
  BitmapDescriptor mosqueMarker = BitmapDescriptor.defaultMarker;
  BitmapDescriptor currentLocationMarker = BitmapDescriptor.defaultMarker;
  BitmapDescriptor myLocationMarker = BitmapDescriptor.defaultMarker;

  // LOCATION DETAIL
  String locality = '';
  String subAdministrativeArea = '';

  // DATE
  String hijriDate = "";
  String gregorianDate = DateFormat('MMM d, yyyy').format(DateTime.now());

  // MOSQUE SLIDE CONTROLLER
  CarouselController slideController = CarouselController();

  // INITIAL LOCATION
  Future<void> initialLocation() async {
    mosques.clear();
    _markers.clear();
    _polylines.clear();

    Future.delayed(Duration(seconds: 1));

    await getCurrentLocation();

    getHijriDate();

    if (this.currentLocation != null) {
      await getMosqueNearby();

      if (selected_mosque_index != null) {
        // select first mosque
        selected_mosque_index = 0;

        // draw polylines
        drawMosquePolyline();
      }
    }
  }

  Future<void> initialCamera() async {
    Location location = Location();
    LocationData location_data = await location.getLocation();
    updateNavigationCamera(location_data);
  }

  Future<void> resetCamera() async {
    mapController!.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(currentLocation!.latitude!, currentLocation!.longitude!),
        zoom: 15,
        tilt: 0,
        bearing: 0.0)));
  }

  Future<void> start_navigation() async {
    initialCamera();
    startTimeNav = DateTime.now();
    onNavigation = true;

    Location location = Location();
    Mosque _mosque = mosques[selected_mosque_index];

    locationSubscription =
        location.onLocationChanged.listen((new_location) async {
      if (onNavigation) {
        currentLocation = new_location;
        updateNavigationCamera(new_location);
        num speed_per_meter = new_location.speed!;

        //  x 3.6 convert from meters/second to km / hour
        num speed_per_km = speed_per_meter * 3.6;

        speed_per_km = speed_per_km > 1 ? speed_per_km : 0;
        speed = speed_per_km.toStringAsFixed(0) + " km/h";

        // update marker position
        _markers.removeWhere(
            (marker) => marker.markerId.value == 'current_location');
        drawMyLocationMarkers();

        // update polyline
        drawMosquePolyline();

        // update distance
        Map<String, dynamic> _distance = MapServices.getDistance(
            new_location.latitude,
            new_location.longitude,
            _mosque.latitude,
            _mosque.longitude);

        distance = _distance["read_distance"];
        num _duration = 0;
        if (speed_per_km != 0) {
          _duration = _distance["distance"] / speed_per_km;
        } else {
          // set 10 km / hour for not moving
          // just for estimate duration
          _duration = _distance["distance"] / 10;
        }

        int _hours = _duration.toInt();
        int _minutes = ((_duration - _hours) * 60).toInt();

        String hours = _hours.toString();
        String minutes = _minutes.toString();

        duration = _hours != 0 ? '$hours hours ' : '';
        duration = duration + '$minutes minutes';

        // if less than 50 meters, then stop navigating
        if (_distance["distance"] < 0.05) {
          // stop navigation
          stop_navigation();
          hasArrived = true;
          endTimeNav = DateTime.now();
          duration_navigate = endTimeNav.difference(startTimeNav);
          read_duration_navigate = formatDuration(duration_navigate.inSeconds);

          await Future.delayed(Duration(seconds: 5));
          hasArrived = false;
        }
      }
    });
  }

  String formatDuration(int durationInSeconds) {
    int hours = durationInSeconds ~/ 3600;
    int minutes = (durationInSeconds % 3600) ~/ 60;
    int seconds = durationInSeconds % 60;

    if (hours > 0) {
      return '${hours}h ${minutes}m ${seconds}s';
    } else if (minutes > 0) {
      return '${minutes}m ${seconds}s';
    } else {
      return '${seconds}s';
    }
  }

  void stop_navigation() {
    locationSubscription.cancel();
    onNavigation = false;
    resetCamera();
    _polylines.clear();
  }

  void updateNavigationCamera(LocationData location) {
    double latitude = location.latitude!;
    double longitude = location.longitude!;
    double heading = location.heading!;

    mapController!.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(latitude, longitude),
        zoom: 19,
        tilt: 90,
        bearing: heading)));
  }

  void drawMosquePolyline() async {
    Mosque _mosque = mosques[selected_mosque_index];
    double latitude = _mosque.latitude;
    double longitude = _mosque.longitude;

    List<LatLng> polylineCoordinates = await MapServices.getPolylines(latitude,
        longitude, currentLocation!.latitude!, currentLocation!.longitude!);

    _polylines.clear();
    _polylines.add(Polyline(
        polylineId: PolylineId('poly'),
        points: polylineCoordinates,
        color: Color(0xFFCBA64B),
        width: onNavigation ? 8 : 4));

    setState(() {});
  }

  void moveMapToMosque() async {
    Mosque _mosque = mosques[card_mosque_index];
    double mosqueLatitude = _mosque.latitude;
    double mosqueLongitude = _mosque.longitude;

    if (mapController != null) {
      mapController!.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(
              target: LatLng(mosqueLatitude + 0.0035, mosqueLongitude),
              zoom: 15,
              tilt: 50)));
    }
  }

  void moveToMyLocation() {
    double? latitude = MapServices.get_user_latitude();
    double? longitude = MapServices.get_user_longitude();
    mapController!.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(latitude! + 0.0035, longitude!), zoom: 15)));
  }

  void moveMapToMosque_by_selectedIndex() async {
    Mosque _mosque = mosques[selected_mosque_index];
    double mosqueLatitude = _mosque.latitude;
    double mosqueLongitude = _mosque.longitude;

    double myLatitude = MapServices.get_user_latitude()!;
    double myLongitude = MapServices.get_user_longitude()!;

    double latitude = (mosqueLatitude + myLatitude) / 2;
    double longitude = (mosqueLongitude + myLongitude) / 2;

    // adjust camera offset
    latitude = latitude + -0.007;

    if (mapController != null) {
      mapController!.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(
              target: LatLng(latitude, longitude), zoom: 16, tilt: 50)));
    }
  }

  Future<void> addCustomIcon() async {
    markerIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "icons/markers/circle.png");

    mosqueMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "icons/markers/mosque.png");
    currentLocationMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "icons/markers/current_location.png");
    myLocationMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "icons/markers/my_location.png");
    setState(() {});
  }

  Future<void> getCurrentLocation() async {
    double? latitude = MapServices.get_user_latitude();
    double? longitude = MapServices.get_user_longitude();

    if (latitude == null || longitude == null) {
      await MapServices.getCurrentLocation();
      latitude = MapServices.get_user_latitude();
      longitude = MapServices.get_user_longitude();
    } else {
      // for initial map location
      LocationData currentLocation = LocationData.fromMap({
        'latitude': latitude,
        'longitude': longitude,
      });

      this.currentLocation = currentLocation;
    }

    // to make sure location
    await MapServices.getCurrentLocation();
    latitude = MapServices.get_user_latitude();
    longitude = MapServices.get_user_longitude();

    LocationData currentLocation = LocationData.fromMap({
      'latitude': latitude,
      'longitude': longitude,
    });

    this.currentLocation = currentLocation;
    moveToMyLocation();

    Map<String, dynamic> address = MapServices.get_user_address();

    // for pray time
    this.currentTimeZone = await FlutterTimezone.getLocalTimezone();

    await getPrayerTime();
    updateTime();

    setState(() {
      this.locality = address['locality'];
      this.subAdministrativeArea = address['subAdministrativeArea'];

      drawMyLocationMarkers();
    });
  }

  void updateTime() async {
    while (true) {
      Duration difference = this.nextPrayerTime.difference(DateTime.now());

      int hours = difference.inHours;
      int minutes = (difference.inMinutes - hours * 60);
      int seconds = (difference.inSeconds - hours * 3600 - minutes * 60);
      if (difference.isNegative) {
        await getPrayerTime();
      }

      setState(() {
        this.diffPrayerTime =
            '-${hours.toString().padLeft(2, '0')}:${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';
      });
      await Future.delayed(Duration(seconds: 1));
    }
  }

  Future<void> getPrayerTime() async {
    final location = tz.getLocation(this.currentTimeZone);

    Coordinates coordinates = Coordinates(
        this.currentLocation!.latitude!, this.currentLocation!.longitude!);

    CalculationParameters params = CalculationMethod.MuslimWorldLeague();
    params.madhab = Madhab.Shafi;

    DateTime date = tz.TZDateTime.from(DateTime.now(), location);
    PrayerTimes prayerTimes =
        PrayerTimes(coordinates, date, params, precision: true);

    // reset prayer time
    this.prayerTimes.clear();

    String next_pray_name = '';
    var time = DateTime.now();
    var next_time;
    var next_time_local;
    for (var i = 0; i < 4; i++) {
      if (next_pray_name == 'isha') {
        DateTime _now = DateTime.now();
        time = DateTime(_now.year, _now.month, _now.day + 1, 3, 0, 0);
        time = tz.TZDateTime.from(time, location);
      }

      // todo
      // fjr time not return nextday. meanwhile time already + 1 day
      next_pray_name = prayerTimes.nextPrayer(date: time);

      // fix if return fajrafter
      next_pray_name = next_pray_name != 'fajrafter' ? next_pray_name : 'fajr';

      next_time = prayerTimes.timeForPrayer(next_pray_name);
      next_time_local = tz.TZDateTime.from(next_time!, location);

      time = next_time_local.add(Duration(minutes: 1));
      if (i == 0) {
        this.nextPrayName =
            next_pray_name[0].toUpperCase() + next_pray_name.substring(1);
        this.nextPrayTime = DateFormat('HH:mm')
            .format(tz.TZDateTime.from(next_time!, location));
      } else {
        this.prayerTimes.add({
          'name': next_pray_name,
          'time': DateFormat('HH:mm')
              .format(tz.TZDateTime.from(next_time_local, location))
        });
      }

      String next = prayerTimes.nextPrayer();
      this.nextPrayerTime =
          tz.TZDateTime.from(prayerTimes.timeForPrayer(next)!, location);
    }
  }

  Future<void> getMosqueNearby() async {
    var mediaQuery = MediaQuery.of(context);
    int screenWidth =
        (mediaQuery.size.width * mediaQuery.devicePixelRatio).toInt();
    double radius = 1000;

    await MapServices.getMosqueNearby(radius, screenWidth);

    List<Mosque> _mosques = MapServices.getMosques();

    drawMosqueMarkers(_mosques);

    Future.delayed(Duration(seconds: 1)).then((value) {
      setState(() {
        mosques = _mosques;

        if (mosques.isNotEmpty) {
          card_mosque_index = 0;
          moveMapToMosque();
        } else {
          noMosqueAround = true;
        }
      });
    });
  }

  void drawMyLocationMarkers() {
    _markers.add(Marker(
        markerId: MarkerId('current_location'),
        icon: selected_mosque_index == null
            ? currentLocationMarker
            : myLocationMarker,
        position: LatLng(
            this.currentLocation!.latitude!, this.currentLocation!.longitude!),
        infoWindow: InfoWindow(title: "Current Location")));
  }

  void drawMosqueMarkers([List<Mosque>? _mosques]) {
    _mosques ??= mosques;

    for (var i = 0; i < _mosques.length; i++) {
      Mosque mosque = _mosques[i];

      _markers.add(Marker(
          markerId: MarkerId(mosque.id),
          icon: selected_mosque_index == null ? mosqueMarker : markerIcon,
          position: LatLng(mosque.latitude, mosque.longitude),
          infoWindow: InfoWindow(title: mosque.name),
          onTap: () {
            if (selected_mosque_index != null) {
              slideController.animateToPage(i, duration: Duration(seconds: 1));
            }
          }));
    }
  }

  void getHijriDate() {
    var _today = HijriCalendar.now();

    this.hijriDate = _today.toFormat("MMMM dd, yyyy") + ' AH\" ';
  }

  Future<void> initializeMap() async {
    await addCustomIcon();
    await getCurrentLocation();
    getHijriDate();

    if (this.currentLocation != null) {
      getMosqueNearby();
    }
  }

  // AnimatedBottomNavigationBar
  var _bottomNavIndex = 0; //default index of a first screen
  final autoSizeGroup = AutoSizeGroup();
  late AnimationController _fabAnimationController;
  late AnimationController _borderRadiusAnimationController;
  late Animation<double> fabAnimation;
  late Animation<double> borderRadiusAnimation;
  late CurvedAnimation fabCurve;
  late CurvedAnimation borderRadiusCurve;
  late AnimationController _hideBottomBarAnimationController;

  final iconList = <SvgPicture>[
    SvgPicture.asset("icons/pray.svg", height: 28, width: 28),
    SvgPicture.asset("icons/profile2.svg", height: 28, width: 28),
  ];

  @override
  void initState() {
    initializeMap();

    super.initState();

    _fabAnimationController = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    _borderRadiusAnimationController = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    fabCurve = CurvedAnimation(
      parent: _fabAnimationController,
      curve: Interval(0.5, 1.0, curve: Curves.fastOutSlowIn),
    );
    borderRadiusCurve = CurvedAnimation(
      parent: _borderRadiusAnimationController,
      curve: Interval(0.5, 1.0, curve: Curves.fastOutSlowIn),
    );

    fabAnimation = Tween<double>(begin: 0, end: 1).animate(fabCurve);
    borderRadiusAnimation = Tween<double>(begin: 0, end: 1).animate(
      borderRadiusCurve,
    );

    _hideBottomBarAnimationController = AnimationController(
      duration: Duration(milliseconds: 200),
      vsync: this,
    );

    Future.delayed(
      Duration(seconds: 1),
      () => _fabAnimationController.forward(),
    );
    Future.delayed(
      Duration(seconds: 1),
      () => _borderRadiusAnimationController.forward(),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (systemBrightness != MediaQuery.of(context).platformBrightness) {
      systemBrightness = MediaQuery.of(context).platformBrightness;
      usePlatformBrightness = true;
    }

    isDarkMode = usePlatformBrightness
        ? MediaQuery.of(context).platformBrightness == Brightness.dark
        : isDarkMode;
    return WillPopScope(
      onWillPop: () async {
        // to be delete if auth

        if (onNavigation) {
          stop_navigation();
          return false;
        } else if (selected_mosque_index != null) {
          selected_mosque_index = null;

          // reset markers
          _markers.clear();
          _polylines.clear();
          drawMyLocationMarkers();
          drawMosqueMarkers();
          return false;
        } else {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) {
            return WellcomeScreen();
          }));
        }
        return true;
      },
      child: Scaffold(
        extendBody: true,
        appBar: selected_mosque_index != null
            ? AppBar(
                leading: IconButton(
                    icon: SvgPicture.asset("icons/arrow_left.svg",
                        colorFilter: ColorFilter.mode(
                            isDarkMode ? Colors.white : Colors.grey.shade900,
                            BlendMode.srcIn),
                        height: 20,
                        width: 20),
                    onPressed: () {
                      if (onNavigation) {
                        stop_navigation();
                      } else {
                        selected_mosque_index = null;

                        // reset markers
                        _markers.clear();
                        _polylines.clear();
                        drawMyLocationMarkers();
                        drawMosqueMarkers();
                      }
                    }),
                title: Row(children: [
                  Spacer(),
                  Row(children: [
                    SvgPicture.asset("icons/notification.svg",
                        colorFilter: ColorFilter.mode(
                            isDarkMode ? Colors.white : Colors.grey.shade900,
                            BlendMode.srcIn),
                        height: 20,
                        width: 20),
                    SizedBox(width: 15),
                    SvgPicture.asset("icons/filter.svg",
                        colorFilter: ColorFilter.mode(
                            isDarkMode ? Colors.white : Colors.grey.shade900,
                            BlendMode.srcIn),
                        height: 20,
                        width: 20)
                  ])
                ]))
            : null,
        body: Stack(
          children: [
            currentLocation != null
                // GOOGLE MAO
                ? GoogleMapWidget(
                    mosques: mosques,
                    location: currentLocation,
                    isDarkMode: isDarkMode,
                    markers: _markers,
                    circles: _circles,
                    polylines: _polylines,
                    selected_mosque_index: selected_mosque_index,
                    onMapCreated: (controller) {
                      mapController = controller;
                    },
                    initialLocation: initialLocation,
                  )
                : Center(child: CircularProgressIndicator()),
            Padding(
              padding: EdgeInsets.only(top: mosques.isNotEmpty ? 50 : 0),
              child: Column(
                children: [
                  // PRAY DETAILS
                  Visibility(
                    visible: selected_mosque_index == null,
                    child: PrayDetails(
                        nextPrayName: nextPrayName,
                        nextPrayTime: nextPrayTime,
                        diffPrayerTime: diffPrayerTime,
                        prayerTimes: prayerTimes,
                        locality: locality,
                        subAdministrativeArea: subAdministrativeArea,
                        hijriDate: hijriDate,
                        gregorianDate: gregorianDate,
                        isDarkMode: isDarkMode,
                        show: mosques.isNotEmpty),
                  ),
                  SizedBox(height: mosques.isNotEmpty ? 10 : 0),
                  // MOSQUES CARD SWIPER
                  MosquesCard(
                    mosques: mosques,
                    moveMapToMosque: (new_index) {
                      this.card_mosque_index = new_index;

                      moveMapToMosque();
                    },
                    selected_mosque_index: selected_mosque_index,
                    navigate: () {
                      selected_mosque_index = card_mosque_index;
                      start_navigation();
                    },
                  )
                ],
              ),
            ),
            Visibility(
                visible: selected_mosque_index != null,
                child: BottomContainer(
                    isDarkMode: isDarkMode,
                    widget: !onNavigation
                        ? !hasArrived
                            ? MosqueSlide(
                                mosques: mosques,
                                selected_mosque_index: selected_mosque_index,
                                isDarkMode: isDarkMode,
                                update_index: (new_index) {
                                  selected_mosque_index = new_index;
                                  drawMosquePolyline();
                                  moveMapToMosque_by_selectedIndex();
                                },
                                navigate: start_navigation,
                                controller: slideController)
                            : selected_mosque_index != null
                                ? HasArrived(
                                    mosque: mosques[selected_mosque_index],
                                    duration: read_duration_navigate,
                                    isDarkMode: isDarkMode)
                                : Container()
                        : NavigationInfo(
                            mosque: mosques[selected_mosque_index],
                            speed: speed,
                            duration: duration,
                            distance: distance,
                            isDarkMode: isDarkMode,
                          ))),
            Visibility(
                visible: noMosqueAround,
                child: Container(
                    color: isDarkMode
                        ? Colors.grey.shade700.withOpacity(0.8)
                        : Colors.grey.shade300.withOpacity(0.8),
                    child: Center(
                      child: Text(
                        "No Mosque Around in radius 10 km",
                        style: TextStyle(
                            color: isDarkMode ? Colors.white : Colors.black,
                            fontSize: 20,
                            fontFamily: "Poppins"),
                      ),
                    )))
          ],
        ),
        floatingActionButton: selected_mosque_index == null
            ? _floatingActionButton(context)
            : null,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar:
            selected_mosque_index == null ? _bottomNavigationBar() : null,
      ),
    );
  }

  AnimatedBottomNavigationBar _bottomNavigationBar() {
    return AnimatedBottomNavigationBar.builder(
      itemCount: iconList.length,
      tabBuilder: (int index, bool isActive) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [iconList[index]],
        );
      },
      backgroundColor: isDarkMode ? Colors.black : Colors.white,
      activeIndex: _bottomNavIndex,
      splashColor: Colors.amber.shade300,
      notchAndCornersAnimation: borderRadiusAnimation,
      splashSpeedInMilliseconds: 300,
      notchSmoothness: NotchSmoothness.smoothEdge,
      gapLocation: GapLocation.center,
      leftCornerRadius: 32,
      rightCornerRadius: 32,
      onTap: (index) => setState(() async {
        _bottomNavIndex = index;

        if (index == 0) {
          usePlatformBrightness = false;
          isDarkMode = !isDarkMode;
        }

        // for test purpose
        // if (index == 0) {
        //   // to make sure location
        //   await MapServices.getCurrentLocation();
        //   double? latitude = MapServices.get_user_latitude();
        //   double? longitude = MapServices.get_user_longitude();

        //   LocationData currentLocation = LocationData.fromMap({
        //     'latitude': latitude,
        //     'longitude': longitude,
        //   });

        //   this.currentLocation = currentLocation;
        //   // moveToMyLocation();
        //   card_mosque_index = 0;
        //   moveMapToMosque();
        // }
      }),
      hideAnimationController: _hideBottomBarAnimationController,
      shadow: BoxShadow(
        offset: Offset(0, 1),
        blurRadius: 12,
        spreadRadius: 0.5,
        color: isDarkMode ? Colors.grey.shade700.withOpacity(0.5) : Colors.grey,
      ),
    );
  }

  Container _floatingActionButton(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: isDarkMode ? Colors.black : Colors.grey,
            blurRadius: 5.0,
          ),
        ],
        gradient: LinearGradient(
          colors: [
            Color.fromARGB(255, 247, 203, 93),
            Color.fromARGB(255, 252, 185, 29),
          ],
        ),
        shape: BoxShape.circle,
      ),
      child: FloatingActionButton(
        child: SvgPicture.asset("icons/navigation2.svg", height: 28, width: 28),
        shape: CircleBorder(side: BorderSide(color: Color(0x8ADAB14D))),
        backgroundColor: Colors.transparent,
        elevation: 0,
        onPressed: () async {
          _fabAnimationController.reset();
          _borderRadiusAnimationController.reset();
          _borderRadiusAnimationController.forward();
          _fabAnimationController.forward();

          await Future.delayed(Duration(milliseconds: 700));
          selected_mosque_index = card_mosque_index;

          // draw polylines
          drawMosquePolyline();
          moveMapToMosque_by_selectedIndex();

          // reset markers
          _markers.clear();
          drawMyLocationMarkers();
          drawMosqueMarkers();
        },
      ),
    );
  }
}
