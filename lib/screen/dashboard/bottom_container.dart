import 'package:flutter/material.dart';
import 'dart:ui';

class BottomContainer extends StatelessWidget {
  final Widget widget;
  final bool isDarkMode;

  BottomContainer({required this.widget, required this.isDarkMode});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50)),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: isDarkMode
                        ? Colors.grey.shade700.withOpacity(0.85)
                        : Colors.white.withOpacity(0.85),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                width: MediaQuery.of(context).size.width,
                child:
                    Padding(padding: const EdgeInsets.all(15.0), child: widget),
              ),
            ),
          )
        ],
      ),
    );
  }
}
