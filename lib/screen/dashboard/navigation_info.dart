import 'package:first_shaf/models/mosque.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';

class NavigationInfo extends StatelessWidget {
  final Mosque mosque;
  final String speed;
  final String duration;
  final String distance;
  final bool isDarkMode;

  NavigationInfo(
      {required this.mosque,
      required this.speed,
      required this.duration,
      required this.distance,
      required this.isDarkMode});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Center(
          child: Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.4,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Color(0xFFBCBCBC),
                      borderRadius: BorderRadius.all(Radius.circular(20))))),
        ),
        Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                      Color(0x8ACBA64B),
                      Color(0x5AFFFFFF)
                    ], // Gradient colors
                        begin: Alignment.topLeft, // Gradient start point
                        end: Alignment.bottomRight // Gradient end point
                        )),
                child: mosque.photo != ''
                    ? CachedNetworkImage(
                        imageUrl: mosque.photo,
                        imageBuilder: (context, imageProvider) => Container(
                              width: 45,
                              height: 45,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover)),
                            ),
                        placeholder: (context, url) => SvgPicture.asset(
                            "icons/mosque.svg",
                            height: 45,
                            width: 45),
                        errorWidget: (context, url, error) => SvgPicture.asset(
                            "icons/mosque.svg",
                            height: 45,
                            width: 45))
                    : SvgPicture.asset("icons/mosque.svg",
                        height: 45, width: 45),
              ),
            ),
            SizedBox(width: 5),
            Expanded(
              child: Container(
                height: 45,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(mosque.name,
                        style: TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 18,
                            color: isDarkMode ? Colors.white : Colors.black,
                            fontWeight: FontWeight.w700)),
                    Text(
                      mosque.address,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:
                              isDarkMode ? Colors.grey[400] : Colors.grey[700],
                          fontSize: 10,
                          fontFamily: "Poppins"),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 5),
        Row(
          children: [
            Text("Speed : ",
                style: TextStyle(
                    fontFamily: "Poppins",
                    color: isDarkMode ? Colors.white : Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w700)),
            Text(speed,
                style: TextStyle(
                    fontFamily: "Poppins",
                    fontSize: 14,
                    color: isDarkMode ? Colors.white : Colors.black)),
          ],
        ),
        Row(
          children: [
            Text("$duration ",
                style: TextStyle(
                    fontFamily: "Poppins",
                    color: isDarkMode ? Colors.white : Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w700)),
            Text("($distance)",
                style: TextStyle(
                    fontFamily: "Poppins",
                    color: isDarkMode ? Colors.white : Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ],
        ),
        SizedBox(height: 5),
        Text("The fastest route with traffic jams",
            style: TextStyle(
                color: isDarkMode ? Colors.grey[400] : Colors.grey[700],
                fontFamily: "Poppins",
                fontSize: 12,
                fontWeight: FontWeight.w200)),
        SizedBox(height: 20),
        Material(
          color: Color.fromARGB(255, 190, 144, 6),
          borderRadius: BorderRadius.circular(50),
          child: Container(
            decoration: BoxDecoration(
                color: Color(0xFFCBA64B),
                border: Border.all(color: Color(0xFFE3C478)),
                borderRadius: BorderRadius.circular(50),
                boxShadow: [
                  BoxShadow(
                      color: isDarkMode
                          ? Colors.grey.shade700.withOpacity(0.5)
                          : Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3))
                ]),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Row(children: [
              Text("On the way",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 14,
                      color: Colors.white,
                      fontWeight: FontWeight.w700)),
              Spacer(),
              SvgPicture.asset("icons/compas.svg", height: 17, width: 17)
            ]),
          ),
        )
      ]),
    );
  }
}
