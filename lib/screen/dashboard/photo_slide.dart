import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PhotoSlide extends StatelessWidget {
  final String mosque_name;
  final List<String> photos;

  PhotoSlide({required this.mosque_name, required this.photos});
  @override
  Widget build(BuildContext context) {
    bool isLight =
        MediaQuery.of(context).platformBrightness == Brightness.light;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: screenWidth < screenHeight
          ? AppBar(
              leading: IconButton(
                  icon: SvgPicture.asset("icons/arrow_left.svg",
                      colorFilter: ColorFilter.mode(
                          isLight ? Colors.grey.shade900 : Colors.white,
                          BlendMode.srcIn),
                      height: 20,
                      width: 20),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }),
              title: Text(
                mosque_name,
                style: TextStyle(
                    fontFamily: "Poppins",
                    color: isLight ? Colors.black : Colors.white),
              ))
          : null,
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: CarouselSlider(
              items: photosWidgets(),
              options: CarouselOptions(
                  autoPlay: false,
                  enableInfiniteScroll: false,
                  aspectRatio: 16 / 9,
                  viewportFraction: 1,
                  onPageChanged: (index, reason) {}))),
    );
  }

  List<Widget> photosWidgets() {
    return photos.map((photo) {
      return CachedNetworkImage(
          imageUrl: photo,
          imageBuilder: (context, imageProvider) => Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: imageProvider, fit: BoxFit.cover)),
              ),
          placeholder: (context, url) =>
              SvgPicture.asset("icons/mosque.svg", height: 60, width: 60),
          errorWidget: (context, url, error) =>
              SvgPicture.asset("icons/mosque.svg", height: 60, width: 60));
    }).toList();
  }
}
