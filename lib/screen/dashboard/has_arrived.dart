import 'package:first_shaf/models/mosque.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HasArrived extends StatelessWidget {
  final Mosque mosque;
  final String duration;
  final bool isDarkMode;

  HasArrived(
      {required this.mosque, required this.duration, required this.isDarkMode});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Center(
          child: Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.4,
                  height: 5,
                  decoration: BoxDecoration(
                      color: Color(0xFFBCBCBC),
                      borderRadius: BorderRadius.all(Radius.circular(20))))),
        ),
        Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                      Color(0x8ACBA64B),
                      Color(0x5AFFFFFF)
                    ], // Gradient colors
                        begin: Alignment.topLeft, // Gradient start point
                        end: Alignment.bottomRight // Gradient end point
                        )),
                child: mosque.photo != ''
                    ? CachedNetworkImage(
                        imageUrl: mosque.photo,
                        imageBuilder: (context, imageProvider) => Container(
                              width: 45,
                              height: 45,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover)),
                            ),
                        placeholder: (context, url) => SvgPicture.asset(
                            "icons/mosque.svg",
                            height: 45,
                            width: 45),
                        errorWidget: (context, url, error) => SvgPicture.asset(
                            "icons/mosque.svg",
                            height: 45,
                            width: 45))
                    : SvgPicture.asset("icons/mosque.svg",
                        height: 45, width: 45),
              ),
            ),
            SizedBox(width: 5),
            Expanded(
              child: Container(
                height: 45,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(mosque.name,
                        style: TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 18,
                            color: isDarkMode ? Colors.white : Colors.black,
                            fontWeight: FontWeight.w700)),
                    Text(
                      mosque.address,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color:
                              isDarkMode ? Colors.grey[400] : Colors.grey[700],
                          fontSize: 10,
                          fontFamily: "Poppins"),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 5),
        Align(
          alignment: Alignment.center,
          child: Text("You Have Arrived",
              style: TextStyle(
                  fontFamily: "Poppins",
                  color: Color(0xFFCBA64B),
                  fontSize: 20,
                  fontWeight: FontWeight.w700)),
        ),
        Align(
          alignment: Alignment.center,
          child: Text("Duration: " + duration,
              style: TextStyle(
                  fontFamily: "Poppins",
                  fontSize: 14,
                  color: isDarkMode ? Colors.white : Colors.black,
                  fontWeight: FontWeight.w700)),
        ),
      ]),
    );
  }
}
