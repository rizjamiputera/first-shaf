import 'package:first_shaf/models/mosque.dart';
import 'package:first_shaf/screen/dashboard/photo_slide.dart';
import 'package:flutter/material.dart';
import 'package:flutter_card_swiper/flutter_card_swiper.dart';
import 'package:first_shaf/screen/mosque/mosque_detail.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MosquesCard extends StatelessWidget {
  final List<Mosque> mosques;
  final Function(int) moveMapToMosque;
  final selected_mosque_index;
  final Function navigate;

  MosquesCard(
      {required this.mosques,
      required this.moveMapToMosque,
      required this.selected_mosque_index,
      required this.navigate});

  @override
  Widget build(BuildContext context) {
    bool shouldScale = mosques.isNotEmpty && selected_mosque_index == null;

    return AnimatedScale(
        duration: Duration(seconds: 2),
        curve: Curves.easeInOutCirc,
        scale: shouldScale ? 1 : 0,
        child: Center(
            child: mosques.isNotEmpty
                ? Container(
                    width: MediaQuery.of(context).size.width - 35,
                    height: (MediaQuery.of(context).size.height * 0.37) + 76,
                    child: CardSwiper(
                      allowedSwipeDirection:
                          AllowedSwipeDirection.only(down: true, up: true),
                      cardsCount: mosques.length,
                      padding: EdgeInsets.all(0),
                      numberOfCardsDisplayed: 3,
                      onSwipe: (previousIndex, currentIndex, direction) {
                        if (currentIndex != null) {
                          // Mosque mosque = mosques[currentIndex];
                          moveMapToMosque(currentIndex);
                        }
                        return true;
                      },
                      cardBuilder: cards,
                    ),
                  )
                : Container()));
  }

  Widget? cards(context, index, percentThresholdX, percentThresholdY) {
    Mosque mosque = mosques[index];
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      height: MediaQuery.of(context).size.height * 0.37,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(40)),
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(40)),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0x8ACBA64B), Color(0x5AFFFFFF)],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight)),
                child: Hero(
                    tag: "mosque" + index.toString(),
                    child: mosque.photo != ''
                        ? CachedNetworkImage(
                            imageUrl: mosque.photo,
                            imageBuilder: (context, imageProvider) =>
                                GestureDetector(
                                  onTap: () {
                                    if (mosque.photos.isNotEmpty) {
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return PhotoSlide(
                                            mosque_name: mosque.name,
                                            photos: mosque.photos);
                                      }));
                                    }
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover)),
                                  ),
                                ),
                            placeholder: (context, url) => SvgPicture.asset(
                                "icons/mosque.svg",
                                height:
                                    MediaQuery.of(context).size.height * 0.6),
                            errorWidget: (context, url, error) =>
                                SvgPicture.asset("icons/mosque.svg",
                                    height: MediaQuery.of(context).size.height *
                                        0.6))
                        : SvgPicture.asset("icons/mosque.svg",
                            height: MediaQuery.of(context).size.height * 0.6)),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                    height: (MediaQuery.of(context).size.height * 0.37) / 2.61,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                          Colors.black.withOpacity(0),
                          Colors.black.withOpacity(0.8)
                        ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter))),
              ),
              Container(
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 20, 20, 30),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(children: [
                            SvgPicture.asset("icons/star.svg",
                                height: 16, width: 16),
                            Text(mosque.rating,
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontSize: 10,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400)),
                            SizedBox(width: 10),
                            SvgPicture.asset("icons/location_white.svg",
                                height: 16, width: 16),
                            Text(mosque.distance,
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400))
                          ]),
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => mosqueDetail(
                                      mosque: mosque,
                                      index: index,
                                      onPopCallback: (do_navigate) {
                                        if (do_navigate) {
                                          navigate();
                                        }
                                      })));
                            },
                            child: Text(
                              mosque.name,
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w700),
                            ),
                          ),
                          Row(children: [
                            mosque.wheelchair_accessible_entrance
                                ? Row(
                                    children: [
                                      SvgPicture.asset(
                                          "icons/wheelchair_white.svg",
                                          height: 17,
                                          width: 17),
                                      SizedBox(width: 5),
                                      Text("Differences",
                                          style: TextStyle(
                                              fontFamily: "Poppins",
                                              color: Colors.white,
                                              fontSize: 10,
                                              fontStyle: FontStyle.normal)),
                                      SizedBox(width: 10),
                                    ],
                                  )
                                : Container(),
                            SvgPicture.asset("icons/parking_white.svg",
                                height: 17, width: 17),
                            SizedBox(width: 5),
                            Text("Parking",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    color: Colors.white,
                                    fontSize: 10,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w400)),
                            SizedBox(width: 10),
                            SvgPicture.asset("icons/translation_white.svg",
                                height: 17, width: 17),
                            SizedBox(width: 5),
                            Text("Translator",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Poppins",
                                    fontSize: 10)),
                          ]),
                          SizedBox(height: 5),
                          Text(
                            mosque.address,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 10,
                                fontFamily: "Poppins"),
                          ),
                        ]),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
