import 'package:flutter/material.dart';

class PrayDetails extends StatelessWidget {
  final String nextPrayName;
  final String nextPrayTime;
  final String diffPrayerTime;
  final bool isDarkMode;
  final List<Map<String, dynamic>> prayerTimes;
  final String locality;
  final String subAdministrativeArea;
  final String hijriDate;
  final String gregorianDate;
  final bool show;

  PrayDetails(
      {required this.nextPrayName,
      required this.nextPrayTime,
      required this.diffPrayerTime,
      required this.prayerTimes,
      required this.locality,
      required this.subAdministrativeArea,
      required this.hijriDate,
      required this.gregorianDate,
      required this.isDarkMode,
      required this.show});

  @override
  Widget build(BuildContext context) {
    return AnimatedScale(
      duration: Duration(seconds: 2),
      curve: Curves.fastOutSlowIn,
      scale: show ? 1 : 0,
      child: Column(
        children: [
          Center(
              child: Column(
            children: [
              Text(nextPrayName,
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 12,
                      color: isDarkMode ? Colors.white : Colors.black)),
              Text(nextPrayTime,
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      color: isDarkMode ? Colors.white : Colors.black)),
              Text(diffPrayerTime,
                  style: TextStyle(
                      // color: Color(0xFF8E8E8E),
                      fontFamily: "Poppins",
                      fontSize: 10,
                      fontWeight: FontWeight.w200,
                      color: isDarkMode ? Colors.white : Color(0xFF8E8E8E))),
            ],
          )),
          Row(children: [
            Expanded(
                child: Container(
                    child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: prayerTimes.map((pray) {
                String prayName = pray["name"];
                prayName = prayName[0].toUpperCase() + prayName.substring(1);
                return Padding(
                  padding: const EdgeInsets.only(right: 8.0, left: 8.0),
                  child: Column(
                    children: [
                      Text(prayName,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 10,
                              color: isDarkMode ? Colors.white : Colors.black)),
                      Text(pray["time"],
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 11,
                              color: isDarkMode ? Colors.white : Colors.black)),
                    ],
                  ),
                );
              }).toList(),
            ))),
            Expanded(
                child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text("$locality, $subAdministrativeArea",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 10,
                            color: isDarkMode ? Colors.white : Colors.black)),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Text(hijriDate,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 10,
                                color:
                                    isDarkMode ? Colors.white : Colors.black)),
                        Text(gregorianDate,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color:
                                    isDarkMode ? Colors.white : Colors.black)),
                      ],
                    ),
                  ),
                ],
              ),
            ))
          ])
        ],
      ),
    );
  }
}
