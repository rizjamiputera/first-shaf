import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:first_shaf/models/mosque.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:location/location.dart';

class GoogleMapWidget extends StatefulWidget {
  final LocationData? location;
  final List<Mosque> mosques;
  final bool isDarkMode;
  final Set<Marker> markers;
  final Set<Circle> circles;
  final Set<Polyline> polylines;
  final selected_mosque_index;

  // controller as argumet
  // so parent can use controller for move camer, change style, etc
  final Function(GoogleMapController) onMapCreated;
  final Function initialLocation;

  GoogleMapWidget(
      {required this.mosques,
      required this.isDarkMode,
      required this.location,
      required this.markers,
      required this.circles,
      required this.polylines,
      required this.onMapCreated,
      required this.selected_mosque_index,
      required this.initialLocation});

  @override
  _GoogleMapWidgetState createState() => _GoogleMapWidgetState();
}

class _GoogleMapWidgetState extends State<GoogleMapWidget>
    with WidgetsBindingObserver {
  String mapLightStyle = '';
  String mapDarkStyle = '';
  late GoogleMapController mapController;

  @override
  void initState() {
    super.initState();

    loadMapStyle();

    // Enable brightness change detection
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    mapController.dispose();
    super.dispose();
  }

  @override
  void didChangePlatformBrightness() {
    bool isDarkMode =
        WidgetsBinding.instance.platformDispatcher.platformBrightness ==
            Brightness.dark;
    setState(() {
      mapController.setMapStyle(isDarkMode ? mapDarkStyle : mapLightStyle);
    });
  }

  Future<void> loadMapStyle() async {
    String lightStyle = await DefaultAssetBundle.of(context)
        .loadString('assets/light_map_style.json');
    String darkStyle = await DefaultAssetBundle.of(context)
        .loadString('assets/dark_map_style.json');
    this.mapLightStyle = lightStyle;
    this.mapDarkStyle = darkStyle;
  }

  @override
  Widget build(BuildContext context) {
    LocationData location = widget.location!;
    List<Mosque> mosques = widget.mosques;
    bool isDarkMode = widget.isDarkMode;
    Set<Marker> markers = widget.markers;
    Set<Circle> circles = widget.circles;
    Set<Polyline> polylines = widget.polylines;
    var selected_mosque_index = widget.selected_mosque_index;
    Function(GoogleMapController) onMapCreated = widget.onMapCreated;
    Function initialLocation = widget.initialLocation;

    double firstHeight = selected_mosque_index == null
        ? (MediaQuery.of(context).size.height * (mosques.isEmpty ? 0 : 0.5))
        : 0;
    double mapHeight = mosques.isEmpty
        ? MediaQuery.of(context).size.height
        : (MediaQuery.of(context).size.height * 0.5);

    // EdgeInsets bottom_pading = selected_mosque_index != null
    //     ? EdgeInsets.only(right: 10, bottom: 10)
    //     : EdgeInsets.only(right: 10, bottom: 65);

    EdgeInsets edgeInsets = selected_mosque_index != null
        ? EdgeInsets.only(right: 10, top: 10)
        : EdgeInsets.only(right: 10, bottom: 65);

    double screenWidth = MediaQuery.of(context).size.width;
    double silhouette_width = screenWidth / 393;
    double silhouette_height = (silhouette_width * 171) + 16;

    return Column(
      children: [
        AnimatedContainer(
          curve: Curves.fastOutSlowIn,
          duration: Duration(seconds: 2),
          width: MediaQuery.of(context).size.width,
          height: mosques.isEmpty || selected_mosque_index != null
              ? 0
              : silhouette_height,
          child: ClipRRect(
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: silhouette_height,
                child: Stack(
                  children: [
                    Transform.scale(
                        scale: isDarkMode ? 1.1 : 3,
                        child: isDarkMode
                            ? ColorFiltered(
                                colorFilter: ColorFilter.mode(
                                    Colors.grey, BlendMode.saturation),
                                child: Image.asset(
                                  "images/dark_cloud.png",
                                  width: MediaQuery.of(context).size.width,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : Image.asset(
                                "images/light_cloud.png",
                                width: MediaQuery.of(context).size.width,
                                fit: BoxFit.cover,
                              )),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Transform.scale(
                        scale: 1.01,
                        child: isDarkMode
                            ? SvgPicture.asset(
                                "images/mosque_silhouette_dark.svg",
                                width: MediaQuery.of(context).size.width)
                            : SvgPicture.asset("images/mosque_silhouette.svg",
                                width: MediaQuery.of(context).size.width),
                      ),
                    )
                  ],
                )),
          ),
        ),
        AnimatedContainer(
          curve: Curves.fastOutSlowIn,
          duration: Duration(seconds: 2),
          color: isDarkMode ? Colors.black : Colors.white,
          width: MediaQuery.of(context).size.width,
          height: firstHeight != 0 ? firstHeight - silhouette_height : 0,
        ),
        AnimatedContainer(
          curve: Curves.fastOutSlowIn,
          duration: Duration(seconds: 2),
          width: MediaQuery.of(context).size.width,
          height: selected_mosque_index == null
              ? mapHeight
              : MediaQuery.of(context).size.height - 92, // reduce for title
          child: Stack(
            children: [
              GoogleMap(
                  // myLocationButtonEnabled: true,
                  // myLocationEnabled: true,
                  zoomControlsEnabled: false,
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                      target: LatLng(
                          location.latitude! - 0.003, location.longitude!),
                      zoom: 15),
                  onMapCreated: (GoogleMapController controller) {
                    mapController = controller;

                    mapController
                        .setMapStyle(isDarkMode ? mapDarkStyle : mapLightStyle);

                    // pass controller to parent
                    onMapCreated(mapController);
                  },
                  markers: markers,
                  circles: circles,
                  polylines: polylines),
              Container(
                  height: mosques.isEmpty || selected_mosque_index != null
                      ? 0
                      : 100,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                    isDarkMode ? Colors.black : Colors.white,
                    isDarkMode
                        ? Colors.black.withOpacity(0)
                        : Colors.white.withOpacity(0)
                  ], begin: Alignment.topCenter, end: Alignment.bottomCenter))),
              Align(
                alignment: selected_mosque_index != null
                    ? Alignment.topRight
                    : Alignment.bottomRight,
                child: Padding(
                  padding: edgeInsets,
                  child: GestureDetector(
                    onTap: () => initialLocation(),
                    child: Container(
                      height: 35,
                      width: 35,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: isDarkMode ? Colors.grey[400] : Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: isDarkMode
                                    ? Colors.grey.shade700.withOpacity(0.5)
                                    : Colors.grey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 5,
                                offset: Offset(0, 3))
                          ]),
                      child: Icon(
                        Icons.my_location,
                        color: Colors.grey[800],
                        size: 20,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
