import 'package:first_shaf/screen/dashboard/dashboard.dart';
import 'package:first_shaf/screen/welcome.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginScreen extends StatelessWidget {
  final String login_statement =
      "By clicking 'Log In', you agress to our terms.\nLearn how e process your data in our privacy and cookie polcies.";
  final String problem_sign_in = "There was a problem logging in?";

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) {
          return WellcomeScreen();
        }));
        return true;
      },
      child: Material(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0x8ACBA64B),
                        Color(0x5AFFFFFF)
                      ], // Gradient colors
                      begin: Alignment.topLeft, // Gradient start point
                      end: Alignment.bottomRight, // Gradient end point
                    ),
                  )),
              SvgPicture.asset("icons/FirstShaf_silhouette.svg",
                  width: MediaQuery.of(context).size.width),
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 2.5,
                    child: Center(
                        child: SvgPicture.asset("icons/FirstShaf.svg",
                            height: 187, width: 155)),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.6,
                      child: Padding(
                        padding: const EdgeInsets.all(50.0),
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  login_statement,
                                  style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontSize: 13,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              SizedBox(height: 50),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return DashboardScreen();
                                  }));
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width - 30,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: Color(0xFFE4E4E4), width: 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Color(0xFFE4E4E4), // Shadow color
                                          spreadRadius: 3, // Spread radius
                                          blurRadius: 10, // Blur radius
                                          offset: Offset(
                                              0, 3), // Offset of the shadow
                                        ),
                                      ]),
                                  child: Row(children: [
                                    SvgPicture.asset("icons/google.svg",
                                        height: 30, width: 30),
                                    Spacer(),
                                    Text('Sign in with Google',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: "Poppins",
                                            fontSize: 13,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500)),
                                    Spacer(),
                                  ]),
                                ),
                              ),
                              SizedBox(height: 20),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return DashboardScreen();
                                  }));
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width - 30,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: Color(0xFFE4E4E4), width: 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Color(0xFFE4E4E4), // Shadow color
                                          spreadRadius: 3, // Spread radius
                                          blurRadius: 10, // Blur radius
                                          offset: Offset(
                                              0, 3), // Offset of the shadow
                                        ),
                                      ]),
                                  child: Row(children: [
                                    SvgPicture.asset("icons/facebook.svg",
                                        height: 30, width: 30),
                                    Spacer(),
                                    Text('Sign in with Facebook',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: "Poppins",
                                            fontSize: 13,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500)),
                                    Spacer(),
                                  ]),
                                ),
                              ),
                              SizedBox(height: 20),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return DashboardScreen();
                                  }));
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width - 30,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: Color(0xFFE4E4E4), width: 1),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                              Color(0xFFE4E4E4), // Shadow color
                                          spreadRadius: 3, // Spread radius
                                          blurRadius: 10, // Blur radius
                                          offset: Offset(
                                              0, 3), // Offset of the shadow
                                        ),
                                      ]),
                                  child: Row(children: [
                                    Image.asset("icons/email.png",
                                        width: 30, height: 30),
                                    Spacer(),
                                    Text('Sign in with Email',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: "Poppins",
                                            fontSize: 13,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500)),
                                    Spacer(),
                                  ]),
                                ),
                              ),
                              SizedBox(height: 20),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  problem_sign_in,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 13,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
