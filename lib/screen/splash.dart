import 'package:first_shaf/screen/welcome.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 3)).then((value) => Navigator.of(context)
        .pushReplacement(
            MaterialPageRoute(builder: (context) => WellcomeScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(color: Colors.white),
          child: Center(
              child: ColorFiltered(
            colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.2), BlendMode.modulate),
            child: Transform.scale(
              scale: 2,
              child: Image.asset(
                'images/grand_mosque.png',
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
              ),
            ),
          )),
        ),
        Center(child: SvgPicture.asset('icons/FirstShaf.svg', height: 125))
      ],
    ));
  }
}
