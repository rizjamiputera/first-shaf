import 'package:first_shaf/models/mosque.dart';
import 'package:first_shaf/screen/dashboard/photo_slide.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';

class mosqueDetail extends StatelessWidget {
  final Mosque mosque;
  final int index;
  final Function(bool) onPopCallback;

  final List<Map<String, dynamic>> facilities = [
    {"label": "Toilet", "icon": Icons.wc},
    {"label": "Wheelchair", "icon": Icons.accessible},
    {"label": "Parking", "icon": Icons.two_wheeler},
    {"label": "Translate", "icon": Icons.translate},
  ];

  final String description =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla facilisi. Proin dictum accumsan ante, et cursus velit vestibulum nec. Curabitur auctor, tortor et viverra consequat, enim sapien vestibulum nunc, a ultrices massa arcu sit amet nunc. Maecenas viverra nibh justo, ac facilisis libero tristique ut. Quisque nec congue felis. In vehicula, dolor nec vehicula iaculis, elit justo viverra sapien, a fringilla ante nulla eget purus. Vivamus non orci sit amet ligula elementum facilisis. Pellentesque consequat risus nec odio faucibus lacinia. Fusce nec aliquam justo. Pellentesque ullamcorper risus vitae felis bibendum, id dictum ante sagittis. In hac habitasse platea dictumst. Vivamus tincidunt ex eu orci cursus, nec scelerisque dolor dictum. Donec vel augue lectus.\n\nVestibulum ullamcorper justo a eleifend dictum. Quisque id tristique justo, nec dictum justo. Fusce hendrerit gravida facilisis. Duis dapibus lacus id mi dignissim, nec ornare nisi tincidunt. Sed at nibh ut turpis pellentesque congue. Vivamus gravida libero vitae libero varius, at suscipit justo faucibus. Sed vitae egestas ligula. Nulla facilisi. Donec luctus odio vel quam efficitur auctor. Sed malesuada arcu ligula, et venenatis lectus aliquet at. Ut et tellus quis purus congue suscipit a in arcu. Sed eget eros vitae velit finibus rhoncus.";

  mosqueDetail(
      {required this.mosque, required this.index, required this.onPopCallback});

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height - 56;

    bool isLight =
        MediaQuery.of(context).platformBrightness == Brightness.light;

    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
              icon: SvgPicture.asset("icons/arrow_left.svg",
                  colorFilter: ColorFilter.mode(
                      isLight ? Colors.grey.shade900 : Colors.white,
                      BlendMode.srcIn),
                  height: 20,
                  width: 20),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          title: Row(children: [
            Spacer(),
            Row(children: [
              SvgPicture.asset("icons/notification.svg",
                  colorFilter: ColorFilter.mode(
                      isLight ? Colors.grey.shade900 : Colors.white,
                      BlendMode.srcIn),
                  height: 20,
                  width: 20),
              SizedBox(width: 15),
              SvgPicture.asset("icons/filter.svg",
                  colorFilter: ColorFilter.mode(
                      isLight ? Colors.grey.shade900 : Colors.white,
                      BlendMode.srcIn),
                  height: 20,
                  width: 20)
            ])
          ])),
      body: Stack(
        children: [
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color(0x8ACBA64B),
                    Color(0x5AFFFFFF)
                  ], // Gradient colors
                  begin: Alignment.topLeft, // Gradient start point
                  end: Alignment.bottomRight, // Gradient end point
                ),
              )),
          Column(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: screenHeight * 0.4,
                  child: Hero(
                      tag: "mosque" + index.toString(),
                      child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(50),
                              bottomRight: Radius.circular(50)),
                          child: mosque.photo != ""
                              ? CachedNetworkImage(
                                  imageUrl: mosque.photo,
                                  imageBuilder: (context, imageProvider) =>
                                      GestureDetector(
                                        onTap: () {
                                          if (mosque.photos.isNotEmpty) {
                                            Navigator.push(context,
                                                MaterialPageRoute(
                                                    builder: (context) {
                                              return PhotoSlide(
                                                  mosque_name: mosque.name,
                                                  photos: mosque.photos);
                                            }));
                                          }
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: imageProvider,
                                                  fit: BoxFit.cover)),
                                        ),
                                      ),
                                  placeholder: (context, url) =>
                                      SvgPicture.asset("icons/mosque.svg",
                                          height: 60, width: 60),
                                  errorWidget: (context, url, error) =>
                                      SvgPicture.asset("icons/mosque.svg",
                                          height: 60, width: 60))
                              : SvgPicture.asset("icons/mosque.svg",
                                  height: 60, width: 60)))),
              SizedBox(height: 80),
              Expanded(
                child: SingleChildScrollView(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Description",
                                    style: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SvgPicture.asset("icons/arrow.svg",
                                      height: 23, width: 23)
                                ]),
                            SizedBox(height: 10),
                            // Address
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Address",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                ),
                                Text(
                                  mosque.address,
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 14,
                                      fontWeight: FontWeight.w200),
                                ),
                                SizedBox(height: 10),
                              ],
                            ),
                            // Phone number
                            Visibility(
                              visible: mosque.phone_number != "",
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Phone",
                                    style: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Text(
                                    mosque.phone_number,
                                    style: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 14,
                                        fontWeight: FontWeight.w200),
                                  ),
                                  SizedBox(height: 10),
                                ],
                              ),
                            ),
                            // REVIEWS
                            Visibility(
                              visible: mosque.reviews.isNotEmpty,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Reviews",
                                    style: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  ReviewWidget(mosque: mosque),
                                  SizedBox(height: 10),
                                ],
                              ),
                            ),
                            Center(
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, bottom: 10),
                                child: Container(
                                    color: Color(0xFFBCBCBC),
                                    width:
                                        MediaQuery.of(context).size.width * 0.4,
                                    height: 2),
                              ),
                            ),
                            Text(
                              "Sharing Experiences",
                              style: TextStyle(
                                  fontFamily: "Poppins",
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500),
                            ),
                            SizedBox(height: 10),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.7,
                              child: Text(
                                  "In your oppinion, how is the experience given in this plcase?",
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 14,
                                      fontWeight: FontWeight.w200)),
                            ),
                            SizedBox(height: 5),
                            Row(
                              children: [
                                SvgPicture.asset("icons/star2.svg",
                                    height: 35, width: 35),
                                SvgPicture.asset("icons/star2.svg",
                                    height: 35, width: 35),
                                SvgPicture.asset("icons/star2.svg",
                                    height: 35, width: 35),
                                SvgPicture.asset("icons/star2.svg",
                                    height: 35, width: 35),
                                SvgPicture.asset("icons/star2.svg",
                                    height: 35, width: 35),
                              ],
                            ),
                            SizedBox(height: 15),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: Row(children: [
                                Expanded(
                                  child: Material(
                                    color: Color.fromARGB(255, 190, 144, 6),
                                    borderRadius: BorderRadius.circular(50),
                                    child: InkWell(
                                      onTap: () {
                                        onPopCallback(true);
                                        Navigator.of(context).pop();
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: Color(0xFFCBA64B),
                                            border: Border.all(
                                                color: Color(0xFFE3C478)),
                                            borderRadius:
                                                BorderRadius.circular(50)),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 20),
                                        child: Row(children: [
                                          Text(
                                            "Navigate",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 14,
                                                color: Colors.white,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w700),
                                          ),
                                          Spacer(),
                                          Image.asset(
                                            "icons/vector.png",
                                            width: 17,
                                            height: 17,
                                          )
                                        ]),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 20),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      20.0, 8.0, 8.0, 8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color: Colors.grey, width: 1),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: SvgPicture.asset("icons/heart.svg",
                                          height: 28, width: 28),
                                    ),
                                  ),
                                )
                              ]),
                            )
                          ]),
                    ),
                  ),
                ),
              )
            ],
          ),
          MosqueCard(mosque: mosque),
        ],
      ),
    );
  }
}

class ReviewWidget extends StatelessWidget {
  const ReviewWidget({
    super.key,
    required this.mosque,
  });

  final Mosque mosque;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: mosque.reviews.map((review) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.white,
                  backgroundImage: NetworkImage(review["profile_photo_url"]),
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      review["author_name"],
                      style: TextStyle(
                          fontFamily: "Poppins",
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                    Row(
                      children: [
                        Row(
                          children: List.generate(
                              review["rating"],
                              (index) => Icon(Icons.star,
                                  color: Colors.amber, size: 14)),
                        ),
                        SizedBox(width: 10),
                        Text(
                          review["relative_time_description"],
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 12,
                              color: Colors.grey[700]),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 5),
            Text(
              review["text"],
              textAlign: TextAlign.justify,
              style: TextStyle(
                  fontFamily: "Poppins",
                  fontSize: 14,
                  fontWeight: FontWeight.w200),
            ),
            SizedBox(height: 10),
          ],
        );
      }).toList(),
    );
  }
}

class MosqueCard extends StatelessWidget {
  const MosqueCard({
    super.key,
    required this.mosque,
  });

  final Mosque mosque;

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height - 56;

    return Column(
      children: [
        SizedBox(
          height: screenHeight * 0.4 - 90,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Container(
            width: screenHeight * 0.8,
            height: 150,
            decoration: BoxDecoration(
                color: Color(0xFFF9F5EB),
                borderRadius: BorderRadius.all(Radius.circular(31)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3))
                ]),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(children: [
                SizedBox(height: 10),
                Text(
                  mosque.name,
                  style: TextStyle(
                    fontFamily: "Poppins",
                    fontSize: 18,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.fromLTRB(50.0, 0, 50.0, 0),
                  child: Row(children: [
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset("icons/star.svg",
                            height: 16, width: 16),
                        SizedBox(width: 5),
                        Text(mosque.rating,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 10,
                                color: Colors.black,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w400))
                      ],
                    )),
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset("icons/location.svg",
                            height: 16, width: 16),
                        SizedBox(width: 5),
                        Text(mosque.distance,
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 10,
                                color: Colors.black,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w400))
                      ],
                    )),
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset("icons/login2.svg",
                            height: 16, width: 16),
                        SizedBox(width: 5),
                        Text(mosque.openNow ? "Open" : "Close",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                color: Colors.black,
                                fontSize: 10))
                      ],
                    )),
                  ]),
                ),
                SizedBox(height: 20),
                Row(children: [
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset("icons/wc.svg", height: 23, width: 23),
                      SizedBox(width: 5),
                      Text("Toilet",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 10,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400))
                    ],
                  )),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset("icons/wheelchair.svg",
                          colorFilter: ColorFilter.mode(
                              mosque.wheelchair_accessible_entrance
                                  ? Color(0xFFD4AF37)
                                  : Colors.grey,
                              BlendMode.srcIn)),
                      SizedBox(width: 5),
                      Text("Differences",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 10,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400))
                    ],
                  )),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset("icons/parking.svg",
                          height: 23, width: 23),
                      SizedBox(width: 5),
                      Text("Parking",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 10,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400))
                    ],
                  )),
                  Expanded(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset("icons/translation.svg",
                          height: 23, width: 23),
                      SizedBox(width: 5),
                      Text("Translator",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              color: Colors.black,
                              fontSize: 10))
                    ],
                  )),
                ])
              ]),
            ),
          ),
        ),
      ],
    );
  }
}
