import 'dart:ui';
import 'dart:async';
import 'package:first_shaf/models/mosque.dart';
import 'package:first_shaf/screen/dashboard/dashboard.dart';
import 'package:first_shaf/screen/mosque/mosque_detail.dart';
import 'package:first_shaf/services/map_services.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:location/location.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MapScreen extends StatefulWidget {
  // const _MapScreen({key? key}) : super(key: key);
  @override
  _MapScreen createState() => _MapScreen();
}

class _MapScreen extends State<MapScreen> {
  LatLng sourceLocation = LatLng(0, 0);
  List<Mosque> mosques = [];

  int _currentSlideIndex = 0;
  Completer<GoogleMapController> _mapController = Completer();
  Set<Marker> _markers = {};
  Set<Polyline> _polylines = {};
  Set<Circle> _circles = {};

  LocationData? currentLocation;
  void _moveToLocation(LatLng location, {bool animate = true}) async {
    final GoogleMapController controller = await _mapController.future;
    animate
        ? controller.animateCamera(CameraUpdate.newLatLngZoom(location, 15))
        : controller.moveCamera(CameraUpdate.newLatLngZoom(location, 15));
  }

  void getPolyPoints(double latitude, double longitude) async {
    // PolylinePoints polylinePoints = PolylinePoints();
    LatLng source =
        LatLng(currentLocation!.latitude!, currentLocation!.longitude!);
    LatLng dest = LatLng(latitude, longitude);

    PointLatLng sourcePoint = PointLatLng(source.latitude, source.longitude);
    PointLatLng destPoint = PointLatLng(dest.latitude, dest.longitude);

    PolylinePoints polylinePoints = PolylinePoints();
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
        "AIzaSyAiEMqQ1t1EnvoJZ7262aK6DHSiKKQbulA", sourcePoint, destPoint,
        travelMode: TravelMode.driving);

    List<LatLng> polylineCoordinates = [];

    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }

    _polylines.clear();
    _polylines.add(Polyline(
        polylineId: PolylineId('poly'),
        points: polylineCoordinates,
        color: Color(0xFFCBA64B),
        width: 4));

    setState(() {});
  }

  void getCurrentLocation() {
    double latitude = MapServices.get_user_latitude()!;
    double longitude = MapServices.get_user_longitude()!;

    currentLocation = LocationData.fromMap({
      'latitude': latitude,
      'longitude': longitude,
    });

    if (currentLocation != null) {
      setState(() {
        this.currentLocation = currentLocation;

        double latitude = currentLocation!.latitude!;
        double longitude = currentLocation!.longitude!;

        _moveToLocation(
            LatLng(currentLocation!.latitude! - 0.005,
                currentLocation!.longitude!),
            animate: false);

        // getPolyPoints();
        _markers.add(Marker(
            markerId: MarkerId('currentLocation'),
            // icon: markerIcon,
            position: LatLng(latitude, longitude),
            infoWindow: InfoWindow(title: "My Curren Location")));

        _circles.add(Circle(
            circleId: const CircleId('mosque'),
            center: LatLng(latitude, longitude),
            radius: 400,
            strokeWidth: 1,
            strokeColor: Color(0xFFCBA64B).withOpacity(0.6),
            fillColor: Color(0xFFCBA64B).withOpacity(0.2)));
        _circles.add(Circle(
            circleId: const CircleId('mosque_top'),
            center: LatLng(latitude, longitude),
            radius: 1000,
            strokeWidth: 1,
            strokeColor: Color(0xFFCBA64B).withOpacity(0.6),
            fillColor: Color(0xFFCBA64B).withOpacity(0.2)));
      });
    }
  }

  void getMosqueNearby() {
    List<Mosque> _mosques = MapServices.getMosques();

    this.mosques = _mosques;

    for (var i = 0; i < _mosques.length; i++) {
      Mosque mosque = _mosques[i];

      setState(() {
        _markers.add(Marker(
            markerId: MarkerId(mosque.id),
            icon: markerIcon,
            position: LatLng(mosque.latitude, mosque.longitude),
            infoWindow: InfoWindow(title: mosque.name)));
      });

      if (i == 0) {
        _moveToLocation(LatLng(mosque.latitude - 0.005, mosque.longitude));
        getPolyPoints(mosque.latitude, mosque.longitude);
      }
    }
  }

  BitmapDescriptor markerIcon = BitmapDescriptor.defaultMarker;
  Future<void> addCustomIcon() async {
    markerIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), "icons/markers/circle.png");
    setState(() {});
  }

  Future<void> initializeMap() async {
    await addCustomIcon();
    getCurrentLocation();

    if (this.currentLocation != null) {
      getMosqueNearby();
    }

    // point map on current location
    sourceLocation = LatLng(
        this.currentLocation!.latitude!, this.currentLocation!.longitude!);
  }

  @override
  void initState() {
    initializeMap();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool isLight =
        MediaQuery.of(context).platformBrightness == Brightness.light;

    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) {
          return DashboardScreen();
        }));
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: SvgPicture.asset("icons/arrow_left.svg",
                  colorFilter: ColorFilter.mode(
                      isLight ? Colors.grey.shade900 : Colors.white,
                      BlendMode.srcIn),
                  height: 20,
                  width: 20),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          title: Row(children: [
            Spacer(),
            Row(children: [
              SvgPicture.asset("icons/notification.svg",
                  colorFilter: ColorFilter.mode(
                      isLight ? Colors.grey.shade900 : Colors.white,
                      BlendMode.srcIn),
                  height: 20,
                  width: 20),
              SizedBox(width: 15),
              SvgPicture.asset("icons/filter.svg",
                  colorFilter: ColorFilter.mode(
                      isLight ? Colors.grey.shade900 : Colors.white,
                      BlendMode.srcIn),
                  height: 20,
                  width: 20)
            ])
          ]),
        ),
        body: Stack(
          children: [
            _googleMaps(context),
            search(),
            mosque_list(),
            // box()
          ],
        ),
      ),
    );
  }

  Widget _googleMaps(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: GoogleMap(
        zoomControlsEnabled: false,
        mapType: MapType.normal,
        initialCameraPosition:
            CameraPosition(target: sourceLocation, zoom: 14.5),
        onMapCreated: (GoogleMapController controller) {
          _mapController.complete(controller);
        },
        markers: _markers,
        polylines: _polylines,
        circles: _circles,
      ),
    );
  }

  Widget search() {
    return Align(
      alignment: Alignment.topCenter,
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(30)),
                boxShadow: [
                  BoxShadow(
                    color: Color(0xFFE4E4E4), // Shadow color
                    spreadRadius: 3, // Spread radius
                    blurRadius: 10, // Blur radius
                    offset: Offset(0, 3), // Offset of the shadow
                  ),
                ]),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              child: Row(
                children: [
                  SvgPicture.asset("icons/list_of_two.svg",
                      height: 8, width: 8),
                  SizedBox(width: 10),
                  Expanded(
                    child: Text("Type Search Mosque",
                        style: TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 14,
                            color: Color(0xFFA5A5A5))),
                  ),
                  Icon(Icons.search, color: Color(0xFFA5A5A5)),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Container(
                      height: 21,
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(
                                  color: Color(0xFFD3D3D3), width: 1))),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("04",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontSize: 16,
                                    height: 1,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFA5A5A5))),
                            Text("Mosque",
                                style: TextStyle(
                                    fontFamily: "Poppins",
                                    fontSize: 8,
                                    height: 1,
                                    color: Color(0xFFA5A5A5)))
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )),
      ),
    );
  }

  Widget mosque_list() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50), topRight: Radius.circular(50)),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.85),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50))),
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: Container(
                              width: MediaQuery.of(context).size.width * 0.4,
                              height: 5,
                              decoration: BoxDecoration(
                                  color: Color(0xFFBCBCBC),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))))),
                      Text("Mosque around you",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 16,
                              fontWeight: FontWeight.w400)),
                      Text("Select the mosque you want to visit",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 12,
                              fontWeight: FontWeight.w200)),
                      mosques.isNotEmpty
                          ? CarouselSlider(
                              options: CarouselOptions(
                                  autoPlay: false,
                                  enableInfiniteScroll: false,
                                  aspectRatio: 3,
                                  onPageChanged: (index, reason) {
                                    setState(() {
                                      _currentSlideIndex = index;
                                      Mosque mosque_data =
                                          mosques[_currentSlideIndex];

                                      getPolyPoints(mosque_data.latitude,
                                          mosque_data.longitude);

                                      _moveToLocation(LatLng(
                                          mosque_data.latitude - 0.005,
                                          mosque_data.longitude));
                                    });
                                  }),
                              items: buildMosqueWidgets(),
                            )
                          : Container(
                              height: 100,
                              child: Center(child: Text("Loading.."))),
                      mosques.isEmpty
                          ? Container()
                          : Container(
                              width: MediaQuery.of(context).size.width * 0.8,
                              child: Row(children: [
                                Expanded(
                                  child: Material(
                                    color: Color.fromARGB(255, 190, 144, 6),
                                    borderRadius: BorderRadius.circular(50),
                                    child: InkWell(
                                      onTap: () {},
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: Color(0xFFCBA64B),
                                            border: Border.all(
                                                color: Color(0xFFE3C478)),
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.grey
                                                      .withOpacity(0.5),
                                                  spreadRadius: 2,
                                                  blurRadius: 5,
                                                  offset: Offset(0, 3))
                                            ]),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 20),
                                        child: Row(children: [
                                          Text("Build a Route",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontFamily: "Poppins",
                                                  fontSize: 14,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w700)),
                                          Spacer(),
                                          Image.asset("icons/vector.png",
                                              width: 17, height: 17)
                                        ]),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      20.0, 8.0, 8.0, 8.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            color: Colors.grey, width: 1),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: SvgPicture.asset("icons/heart.svg",
                                          height: 28, width: 28),
                                    ),
                                  ),
                                )
                              ]),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> buildMosqueWidgets() {
    return mosques.map((mosque) {
      int index = mosques.indexOf(mosque);
      return GestureDetector(
        onTap: () {
          // int index = mosques.indexOf(mosque);
          // print(index);

          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => mosqueDetail(
                    mosque: mosque,
                    index: index,
                    onPopCallback: (navigate) {},
                  )));
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Container(
                height: 80,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(13)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 5,
                          offset: Offset(0, 3))
                    ]),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Container(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                      Color(0x8ACBA64B),
                                      Color(0x5AFFFFFF)
                                    ], // Gradient colors
                                        begin: Alignment
                                            .topLeft, // Gradient start point
                                        end: Alignment
                                            .bottomRight // Gradient end point
                                        )),
                                child: Hero(
                                    tag: "mosque" +
                                        mosques.indexOf(mosque).toString(),
                                    child: mosque.photo != ''
                                        ? CachedNetworkImage(
                                            imageUrl: mosque.photo,
                                            imageBuilder: (context,
                                                    imageProvider) =>
                                                Container(
                                                  width: 60,
                                                  height: 60,
                                                  decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                          image: imageProvider,
                                                          fit: BoxFit.cover)),
                                                ),
                                            placeholder: (context, url) =>
                                                SvgPicture.asset(
                                                    "icons/mosque.svg",
                                                    height: 60,
                                                    width: 60),
                                            errorWidget: (context, url,
                                                    error) =>
                                                SvgPicture.asset(
                                                    "icons/mosque.svg",
                                                    height: 60,
                                                    width: 60))
                                        : SvgPicture.asset("icons/mosque.svg",
                                            height: 60, width: 60)),
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              height: 60,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    mosque.name,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 13,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  Align(
                                    alignment: Alignment.centerLeft,
                                    child: Row(
                                      children: [
                                        SvgPicture.asset("icons/star.svg",
                                            height: 16, width: 16),
                                        Text(mosque.rating,
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 10)),
                                        SizedBox(width: 5),
                                        SvgPicture.asset("icons/location.svg"),
                                        Text(
                                            // mosque["distance"].toString(),
                                            "10 km",
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 10)),
                                        SizedBox(width: 5),
                                        SvgPicture.asset("icons/login2.svg",
                                            height: 16, width: 16),
                                        Text(mosque.openNow ? "Open" : "Close",
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 10)),
                                      ],
                                    ),
                                  ),
                                  index == 0
                                      ? Expanded(
                                          child: Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Text(
                                            "Nearest",
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                color: Color(0xFFCBA64B),
                                                fontSize: 10,
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ))
                                      : Text("")
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }).toList();
  }
}
