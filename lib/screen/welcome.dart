import 'package:first_shaf/screen/dashboard/dashboard.dart';
// import 'package:first_shaf/screen/mosque/map.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'login.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WellcomeScreen extends StatefulWidget {
  @override
  _WellcomeScreenState createState() => _WellcomeScreenState();
}

class _WellcomeScreenState extends State<WellcomeScreen> {
  List<Map<String, String>> messages = [
    {
      'label': 'Ikhtiar to get closer to Allah',
      'desc':
          'The importance of performing the live daily prayers on time and finding the rights mosque'
    },
    {
      'label': 'Istiqomah makes you stronger',
      'desc': 'Daily time alert makes you well prepared'
    },
    {
      'label': 'Easy to Pray anywhere',
      'desc': 'Find best routes to get to the nearest mosque'
    },
  ];

  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Material(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Stack(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Center(
                      child: ColorFiltered(
                    colorFilter: ColorFilter.mode(
                        Colors.white.withOpacity(0.2), BlendMode.modulate),
                    child: Transform.scale(
                      scale: 2,
                      child: Image.asset(
                        'images/grand_mosque.png',
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                      ),
                    ),
                  )),
                ),
                Column(
                  children: [
                    SizedBox(height: MediaQuery.of(context).size.height / 5),
                    Center(
                        child: SvgPicture.asset("icons/FirstShaf.svg",
                            height: 127)),
                    SizedBox(height: MediaQuery.of(context).size.height / 7),
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: messages.asMap().entries.map((entry) {
                              return GestureDetector(
                                onTap: () {
                                  _controller.animateToPage(entry.key);
                                },
                                child: Container(
                                  width: _current == entry.key ? 36.0 : 12.0,
                                  height: 12.0,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 4.0),
                                  decoration: BoxDecoration(
                                      borderRadius: _current == entry.key
                                          ? BorderRadius.circular(20)
                                          : null,
                                      shape: _current == entry.key
                                          ? BoxShape.rectangle
                                          : BoxShape.circle,
                                      color: (_current == entry.key
                                              ? Color(0xFFCBA64B)
                                              : Colors.white)
                                          .withOpacity(
                                              _current == entry.key ? 0.9 : 1)),
                                ),
                              );
                            }).toList(),
                          ),
                          CarouselSlider(
                            options: CarouselOptions(
                                autoPlay: true,
                                autoPlayInterval: Duration(seconds: 5),
                                viewportFraction: 1,
                                aspectRatio: 16 / 9,
                                onPageChanged: (index, reason) {
                                  setState(() {
                                    _current = index;
                                  });
                                }),
                            items: buildMessageWidgets(),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Row(children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Material(
                                  color: Color(0xFFCBA64B),
                                  borderRadius: BorderRadius.circular(50),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        return DashboardScreen();
                                      }));
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 20),
                                      child: Row(
                                        children: [
                                          Text(
                                            'Get Started',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: "Poppins",
                                                color: Colors.white,
                                                fontSize: 13,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Spacer(),
                                          SvgPicture.asset(
                                              "icons/arrow_right_circle.svg",
                                              height: 20)
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 100),
                            Container(
                              width: screenWidth < 640 ? 80 : 300,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Material(
                                  color: Colors.white.withOpacity(0),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        return LoginScreen();
                                      }));
                                    },
                                    child: Text('Login',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: "Poppins",
                                            fontSize: 13,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                ),
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> buildMessageWidgets() {
    return messages
        .map(
          (item) => Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  child: Text(
                    item['label'] ?? '',
                    maxLines: 2,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "Poppins",
                        fontSize: 25,
                        height: 1.1,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Text(item['desc'] ?? '',
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: "Poppins",
                        fontSize: 13,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w300,
                      )),
                ),
              ],
            ),
          ),
        )
        .toList();
  }
}
